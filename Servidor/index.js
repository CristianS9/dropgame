// Las sessiones multiples no estan activadas, fatla base de datos para almacenar las sessiones



var express = require("express");
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var app = express();
var fs = require("fs");
var bodyParser = require("body-parser");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

// To access static content in folder "Cliente"
app.use(express.static("Cliente"));

//Configure session module
var sess = {
  secret: 'keyboard cat',
   resave: true,
  saveUninitialized: true,
  cookie: {}
}
app.use(session(sess))

app.get("/",function(req,res) {
   res.sendFile(__dirname + '/Cliente/login.html');
});

app.get("/signup",function(req,res){
   res.sendFile(__dirname + "/Cliente/signup.html");
});

app.get("/game",function(req,res){
  if(req.session.connected) {
     res.sendFile(__dirname + "/Cliente/game.html");
  } else {
     res.sendFile(__dirname + '/Cliente/login.html');
  }

});

//Comprobar el usuario y la contraseña estan en la base de datos
app.post("/login",function(req,res){
   var user = req.body.user[0];
   var password = req.body.user[1];

   var comprobar = new Promise(function(resolve,reject){
      fs.readFile(__dirname + "/datos/cuentas.txt","utf8",function(err,data){
         var info = data;
         var busqueda = info.search("@" + user + "@#" + password + "#")
         resolve (busqueda);
      });
   });
   comprobar.then(
      function(busqueda){
         if (busqueda == -1) {                                 // SI los datos no existe o son incorrectos
            res.send(JSON.stringify({code: "noexiste"}));
         } else {                                              //Cuando los datos son correctos
            req.session.connected = true;                      //Confirma el inicio de sesion
            req.session.user = user;                           //Establece el usuario para la session
            res.send(JSON.stringify({code: "existe"}));

         }
      });

});

app.get("/user",function(req,res){
   if(req.session.connected) {
      res.send(JSON.stringify({user: req.session.user}));
   } else {
      res.send(JSON.stringify({user: ""}));
   }
});

app.post("/create",function(req,res){
   var user = req.body.user[0];
   var pass = req.body.user[1];
   var email = req.body.user[2];
   var comprobar = new Promise(function(resolve, reject){
         fs.readFile(__dirname + "/datos/cuentas.txt","utf8",function(err,data){
         var info = data;
         var exiuser = info.search("@" + user + "@" );
         var exipass = info.search("#" + pass + "#");
         var exiemail = info.search("%" + email + "%");
         var cuser = "+";
         if (exiuser == -1) {
            cuser = "-";
         }

         var cpass = "+";
         if (exipass == -1) {
            cpass = "-";
         }

         var cemail = "+";
         if (exiemail == -1) {
            cemail = "-";
         }
         var codigo = cuser + cpass + cemail;
         resolve (codigo);
      });
   });
   comprobar.then(
      function(codigo){
      if ( (codigo == "---") || (codigo == "-+-") ) {
         fs.appendFile("datos/cuentas.txt","@"+ user + "@#" + pass +"#%" + email +"%\n");
         res.send(JSON.stringify({code: "***"}));
      } else {
         res.send(JSON.stringify({code: codigo}));
      }
   });
 });

app.post("/actualizarpuntuacion",function(req,res){
   var user = req.body.info[0];
   var puntuacion = req.body.info[1];
   var actualizar = new Promise (function(resolve,reject){
      fs.readFile(__dirname + "/datos/puntuaciones.txt","utf8",function(err,data){
         var info = data;
         var existe = info.search("@" + user + "@");
         resolve(existe);
      });
   });
   actualizar.then(function(existe){
      if (existe == -1){
         fs.appendFile("datos/puntuaciones.txt","@" + user + "@-%" + puntuacion + "%\n");
      } else {
         var comparar = new Promise (function(resolve,reject){
            fs.readFile(__dirname + "/datos/puntuaciones.txt","utf8",function(err,data){
               var info = data;
               var busqueda = "@" + user + "@-%(\\d*)%" ;
               var regex = new RegExp(busqueda);
               var aplicar = info.match(regex);
               var apuntuacion = aplicar[1];
               resolve(apuntuacion);
            });
         });
         comparar.then(function(apuntuacion){
            if (puntuacion > apuntuacion) {
               var promesacambio = new Promise(function(resolve,reject){
                  fs.readFile(__dirname + "/datos/puntuaciones.txt","utf8",function(err,data) {
                     var info = data;
                     var texto = "@" + user + "@-%\\d*%\\n" ;
                     var regex = new RegExp(texto);
                     var seleccion = info.match(regex)
                     var cambio = info.replace( seleccion , "@" + user + "@-%" + puntuacion + "%\n");
                     resolve(cambio);

                  });
               });
               promesacambio.then(function(cambio){
                  fs.writeFile(__dirname + "/datos/puntuaciones.txt",cambio);
                  res.send(JSON.stringify({code:"record"}));
               });
            } else {
               res.send(JSON.stringify({code:"norecord"}));
            }
         });

      }
   });
});

app.get("/borrarconexion",function(req,res){
  req.session.destroy(function(err) {
  if(err){
    console.log("algo fallo al borrar la conexion");
  }
})
});


app.listen(3000,function() {
   console.log("Serv working");

});
